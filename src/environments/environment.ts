// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // DAYACTIVITY:'http://localhost:62866/api/dayactivities/',
  // SECURITY:'http://localhost:62866/api/security/',
  // LEADERBOARD:'http://localhost:62866/api/leaderboard/',
  // ACTIVIY:'http://localhost:62866/api/activity/',
  // DAY:'http://localhost:62866/api/day/',
  // TEAM:'http://localhost:62866/api/teams/',
  // LOG:'http://localhost:62866/api/log/',

  DAYACTIVITY:'https://localhost:44344/api/dayactivities/',
  SECURITY:'https://localhost:44344/api/security/',
  LEADERBOARD:'https://localhost:44344/api/leaderboard/',
  ACTIVIY:'https://localhost:44344/api/activity/',
  DAY:'https://localhost:44344/api/day/',
  TEAM:'https://localhost:44344/api/teams/',
  LOG:'https://localhost:44344/api/log/',

  // DAYACTIVITY:'https://localhost/api/dayactivities/',
  // SECURITY:'https://localhost/api/security/',
  // LEADERBOARD:'https://localhost/api/leaderboard/',
  // ACTIVIY:'https://localhost/api/activity/',
  // DAY:'https://localhost/api/day/',
  // TEAM:'https://localhost/api/teams/',
  // LOG:'https://localhost/api/log/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
