export const environment = {
  production: true,
  DAYACTIVITY:'https://localhost/api/dayactivities/',
  SECURITY:'https://localhost/api/security/',
  LEADERBOARD:'https://localhost/api/leaderboard/',
  ACTIVIY:'https://localhost/api/activity/',
  DAY:'https://localhost/api/day/',
  TEAM:'https://localhost/api/teams/',
  LOG:'https://localhost/api/log/',
};
