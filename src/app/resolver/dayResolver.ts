import { Resolve } from "@angular/router";
import { Injectable } from "@angular/core";
import { DayModel } from "../models/day";
import { DayService } from "../services/day.service";

@Injectable({
    providedIn: 'root'
  })
  
export class DayResolver implements Resolve<DayModel>{
    constructor(private dayService:DayService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : DayModel | import("rxjs").Observable<DayModel> | Promise<DayModel> { 
        return this.dayService.getById(route.params.id);
    }
}
