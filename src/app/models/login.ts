import { Base64 } from "node-forge";

export interface LoginModel{
    login:string,
    plainPassword:string,
}