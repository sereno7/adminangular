export interface TeamModel{
    id?:number,
    name:string,
    color?:string,
    slogan?:string,
    idDay?:number
}