import { TeamModel } from "./team";
export interface DayModel{
    id:number,
    customerName:string,
    date:string,
    countActivity:number,
    status?:number,
    listTeams:TeamModel[]
}