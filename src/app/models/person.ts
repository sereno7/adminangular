export interface PersonModel{
    id:number,
    name:string,
    idTeam?: number,
    role: string,
    idDay?:number,
    // isAlreadyConnected:boolean;
}
