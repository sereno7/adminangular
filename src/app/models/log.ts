export interface LogModel{
    id:number,
    message:string,
    timeStamp:string,
    exception:string
}