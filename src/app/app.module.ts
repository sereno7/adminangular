import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EventListComponent } from './components/event-list/event-list.component';
import { EventAddComponent } from './components/event-add/event-add.component';
import { DayFormComponent } from './components/day-form/day-form.component';
import { EventUpdateComponent } from './components/event-update/event-update.component';
import { AddTeamComponent } from './components/dialog/add-team/add-team.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { LogComponent } from './components/log/log.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MatSortModule } from '@angular/material/sort';
import { ConfirmDeleteComponent } from './components/dialog/confirm-delete/confirm-delete.component';
import { ChartsModule } from 'ng2-charts';
import { TokenInterceptor } from '../../src/app/interceptor/token.interceptor';
import { HttpRequestInterceptor } from '../../src/app/interceptor/http-request.interceptor';
import {  MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoaderComponent } from './components/loader/loader.component';
import { ActivityComponent } from './components/activity/activity.component';
import { AddActivityComponent } from './components/dialog/add-activity/add-activity.component'
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventAddComponent,
    DayFormComponent,
    EventUpdateComponent,
    AddTeamComponent,
    LogComponent,
    LoginComponent,
    DashboardComponent,
    ConfirmDeleteComponent,
    LoaderComponent,
    ActivityComponent,
    AddActivityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSortModule,
    ChartsModule,
    MatProgressSpinnerModule,
    DataTablesModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
