import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { LoaderComponent } from '../components/loader/loader.component';
import { LoaderService } from '../services/loader.service'


@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  private _requesters = [];
  private startTime;
  constructor(
    public dialog: MatDialog,
    public loaderService : LoaderService,
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
      this.loaderService.show();
      if(this._requesters.length == 0)
      {
        this.startTime = new Date().getTime();
        this._requesters.push(request);
      }
      return next.handle(request).pipe(delay(750 - (new Date().getTime() - this.startTime)),finalize(() => 
      {
        this._requesters.pop();
        if(this._requesters.length == 0)
        {
          this.loaderService.hide();
        }
      }));

      // let dialog = this.dialog.open(LoaderComponent, {
      //   height: '400px',
      //   width: '600px'});
      // return next.handle(request).pipe(finalize(() => {
      //   setTimeout(() => {
      //     dialog.close();
      //   }, 2000)
      // }));
  }
}
