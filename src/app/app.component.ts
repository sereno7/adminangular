import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService } from './services/loader.service';
import { SessionService } from './services/session.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  collapsed: boolean;
  title = 'angular-boilerplate';
  loading: boolean = false;

  constructor(
    public loaderService : LoaderService,
    private sessionService : SessionService,
    private router:Router,
) {
    
  }
  ngOnInit(): void {
    this.collapsed = true;
  }

  logout() {
    if(!this.sessionService.user) return;
    this.sessionService.stop();
    this.router.navigateByUrl('');
  }

  collapse() {
    this.collapsed = true;
  }

  toggle() {
    this.collapsed = !this.collapsed;
  }
}
