import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivityService } from '../services/activity.service';
import { DayService } from '../services/day.service';
import { TeamService } from '../services/team.service';
export class CustomValidators {
    static notBeforeToday(c: FormControl) {
        if(!c.value) return { "select_date": true };
        if(c.value) {
            let date = new Date(c.value);
            let today = new Date();
            if(date.getTime() < today.getTime()) {
                return { "too_short_date": true };
            }
        }
        return null
    }

    static existDate(service: DayService, id: number) {
        return (c: FormControl) => {
            return service.checkDate(c.value, id).pipe(map(data => {
                console.log(data);
                
                if(data) {
                    return { "exist_date": true }
                }
                else{
                    return null;
                }
            }))
        }      
    }

    static existActivity(service: ActivityService) {
        return (c: FormControl) => {
            return service.checkActivity({name:c.value}).pipe(map(data => {
                if(data) {
                    return { "exist_activity": true }
                }
                else{
                    return null;
                }
            }))
        }      
    }

    static existTeam(service: TeamService, id:number) {
        return (c: FormControl) => {

            return service.checkTeam(c.value, id).pipe(map(data => {
                if(data) {
                    return { "exist_team": true }
                }
                else{
                    return null;
                }
            }))
        }      
    }
} 