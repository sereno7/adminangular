import { Component, OnInit } from '@angular/core';
import { LeaderboardService } from 'src/app/services/leaderboard.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Minimum' },
    { data: [], label: 'Moyenne' },
    { data: [], label: 'Maximum' },
  ];

  constructor(
    private leaderboardService:LeaderboardService
    ) { }

  ngOnInit(): void {
    this.leaderboardService.getAllAverage().subscribe(data =>{
      data.forEach(element => {  
        this.barChartLabels.push(element.name);  
        this.barChartData[1]?.data.push(element.averagePoint);
      });     
    })
    this.leaderboardService.getAllMax().subscribe(data =>{
      data.forEach(element => {  
        this.barChartData[2]?.data.push(element.maxPoint);
      });     
    })
    this.leaderboardService.getAllMin().subscribe(data =>{
      data.forEach(element => {  
        this.barChartData[0]?.data.push(element.minPoint);
      });     
    })
  }
}
