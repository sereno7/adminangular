import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DayModel } from '../../models/day';
import { TeamModel } from '../../models/team';
import { DayService } from '../../services/day.service';
import { TeamService } from '../../services/team.service';
import { ActivityService } from '../../services/activity.service';
import { DayActivityService } from '../../services/day-activity.service';
import { CustomValidators } from '../../validators/customValidators';
import { AddTeamComponent } from '../../components/dialog/add-team/add-team.component';
import { ActivityModel } from '../../models/activity';
import { DayActivitiesModel } from '../../models/dayActivities';
import { ConfirmDeleteComponent } from '../dialog/confirm-delete/confirm-delete.component';



@Component({
  selector: 'app-day-form',
  templateUrl: './day-form.component.html',
  styleUrls: ['./day-form.component.scss']
})

export class DayFormComponent implements OnInit {
  displayedColumns: string[] = ['name', 'color', 'delete'];
  dataSource:TeamModel[];
  listDayActivities:DayActivitiesModel[];
  listActivity:ActivityModel[];
  model:DayModel;
  fg : FormGroup;
  allTeam: TeamModel[];
  // date:Date;
  constructor(
    private dayService:DayService,
    private teamService:TeamService,
    private activityService:ActivityService,
    private dayActivitiesService:DayActivityService,
    private router:Router,
    private route: ActivatedRoute,
    public dialog:MatDialog
    ) { }

  ngOnInit(): void {

    this.activityService.getAll().subscribe(data =>{
      this.listActivity = data;
    })
    this.dataSource = this.route.snapshot.data.resolveDay?.listTeams;
    this.model = this.route.snapshot.data.resolveDay;
    this.initForm(this.model); 
    this.dayActivitiesService.getByIdDay(this.fg?.get("id").value).subscribe(data =>{
      this.listDayActivities = data;
    });   
  }

  private initForm(item: DayModel) {
    if(this.fg) return;
    this.fg = new FormGroup({
      id : new FormControl(item?.id || 0),
      customerName : new FormControl(
        { value:item?.customerName , disabled: this.isDisabled(item) }, 
        [Validators.required], 
        []),
      date : new FormControl(
        { value:item?.date , disabled: this.isDisabled(item) },  
        [CustomValidators.notBeforeToday], 
        [CustomValidators.existDate(this.dayService, item?.id || 0)]),
     })
  }

  submit() {
    console.log("TEST");
    
    if(this.fg.invalid) return;
    if(this.fg.value.id){
      this.dayService.update(this.fg.value).subscribe ( x =>  {
        this.router.navigateByUrl("/event-list");
      })}
    else {
      if(this.fg.invalid) return;
      this.dayService.add(this.fg.value).subscribe ( x =>{
        this.fg.get("id").setValue(x);
      })
    }
  }

  isDisabled(item) {
    if(!item?.date) return
    if(this.fg?.get("date").value == undefined) return;
    return new Date(item.date)?.getTime() < new Date().getTime();
  }

  openModalAdd()
  {    
    let dialogRef = this.dialog.open(AddTeamComponent,{
      data: { id: this.fg.get("id").value}});
    dialogRef.afterClosed().subscribe(result => {
      this.teamService.add({name:result.name,idDay:this.fg?.get("id").value, color:result.color}).subscribe(data =>{
        this.dayService.getById(this.fg.get("id").value).subscribe(data =>{
          this.dataSource = data.listTeams;         
          this.model = data;
        })
      })
    });
  }

  delete(id:number){
    console.log("test");
    this.teamService.delete(id).subscribe(data =>{
      this.dayService.getById(this.fg.get("id").value).subscribe(data =>{
        this.model = data;
    });
  })
}

    onCheckboxChange(e,id:number){
      if(e.target.checked)
      {
        this.dayActivitiesService.add({idDay:this.fg.get("id").value, idActivity:id});
        
      }
      else
      {
        this.dayActivitiesService.delete({idDay:this.fg.get("id").value, idActivity:id});
      }
    }

    checked(id:number){
      let i = this.listDayActivities?.find(x => x.idActivity == id);
      if(i)
      {
        return true;
      }
      else
        return false;
    }

    openAlertDialog(id: number) {
      let dialogRef = this.dialog.open(ConfirmDeleteComponent);
      dialogRef.afterClosed().subscribe(result => {
        if(result)
        {
          this.teamService.delete(id).subscribe(data =>{
            this.dayService.getById(this.model.id).subscribe(day =>{
              this.dataSource = day.listTeams;
            })
          })
        }
        else
          return
      });
    };


  }
