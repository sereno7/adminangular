import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivityService } from 'src/app/services/activity.service';
import { CustomValidators } from 'src/app/validators/customValidators';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.scss']
})
export class AddActivityComponent implements OnInit {
  fg:FormGroup;
  constructor(
    private activityService:ActivityService,
    public dialogRef: MatDialogRef<AddActivityComponent>) { }

  ngOnInit(): void {
 
    this.initForm();
  }

  private initForm() {
    if(this.fg) return;
    this.fg = new FormGroup({
      name : new FormControl(null,[Validators.required],[CustomValidators.existActivity(this.activityService)]),
      description : new FormControl(null,[Validators.required]),
      
  })
}

submit(){
  this.dialogRef.close(this.fg.value);
}
}
