import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TeamService} from'../../../services/team.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomValidators } from 'src/app/validators/customValidators';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.scss']
})
export class AddTeamComponent implements OnInit {
  fg:FormGroup;
  constructor(
    private teamService: TeamService,
    public dialogRef: MatDialogRef<AddTeamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {id: number}
   ) { }

  ngOnInit(): void {    
    this.initForm();
  }

  private initForm() {
    if(this.fg) return;
    this.fg = new FormGroup({
      // idDay : new FormControl(this.data.idDay),
      name : new FormControl(null,[Validators.required],[CustomValidators.existTeam(this.teamService, this.data.id)]),
      color : new FormControl(null,[Validators.required])
  })
}

submit(){  
  this.dialogRef.close(this.fg.value);
}
}
