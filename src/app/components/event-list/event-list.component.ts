import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DayModel } from '../../models/day';
import { DayService} from '../../services/day.service'
import { ConfirmDeleteComponent } from '../dialog/confirm-delete/confirm-delete.component';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {
  listDay:DayModel[];
  displayedColumns: string[] = ['date', 'id', 'customerName', 'countTeams','edit', 'delete'];
  dataSource:DayModel[];
  constructor(
    private dayService:DayService,
    private router:Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ConfirmDeleteComponent>
    ) { }

  ngOnInit(): void {
    this.dayService.context.subscribe(data =>{
      this.dataSource = data;  
    })
  }
  edit(day:DayModel){   
    if(this.isDisabled(day.date)) return
    this.router.navigateByUrl("/event-update/" + day.id);
  }

  openAlertDialog(id: number) {
    let dialogRef = this.dialog.open(ConfirmDeleteComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result)
      {
        this.dayService.delete(id).subscribe(data =>{
          this.dayService.refresh();
        })
        
      }
      else
        return
    });
  };

  isDisabled(item:string) {
    return new Date(item)?.getTime() < new Date().getTime();
  }
    

}
