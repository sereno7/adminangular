import { Component, OnInit } from '@angular/core';
import { LogModel } from '../../models/log';
import { LogService } from '../../services/log.service'


@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  options : DataTables.Settings;
  displayedColumns: string[] = ['timeStamp', 'message', 'exception'];
  dataSource:LogModel[];
  constructor(private serviceLog: LogService) { }

  ngOnInit() {
   this.serviceLog.getAll().subscribe(data =>{
     this.dataSource = data;  
   })
   this.options = {
    language : { url : "https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json" },
    responsive : true,
  }
  }
}
