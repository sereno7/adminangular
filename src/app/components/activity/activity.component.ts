import { Component, OnInit } from '@angular/core';
import { ActivityModel } from '../../models/activity';
import { MatDialog } from '@angular/material/dialog';
import { AddActivityComponent } from '../dialog/add-activity/add-activity.component';
import { ActivityService } from '../../services/activity.service';
import { ConfirmDeleteComponent } from '../dialog/confirm-delete/confirm-delete.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  displayedColumns: string[] = ['name', 'description', 'delete'];
  dataSource:ActivityModel[];
  constructor(
    public dialog: MatDialog,
    private activityService : ActivityService
    ) { }

  ngOnInit(): void {
    this.activityService.context.subscribe(data =>{
      this.dataSource = data;
    })
  }
  openAdd(){
    let dialogData = this.dialog.open(AddActivityComponent);
    dialogData.afterClosed().subscribe(data =>{
      this.activityService.add(data).subscribe(data =>{
        this.activityService.refresh();
      })    
    })
  }

  delete(id: number) {
    let dialogRef = this.dialog.open(ConfirmDeleteComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result)
      {    
        this.activityService.delete(id).subscribe(data =>{
          this.activityService.refresh();
      })
      }   
      else
        return
  })
}
}
