import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { SessionService } from '../../services/session.service'
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginModel } from '../../models/login';
// import * as JsEncryptModule from 'jsencrypt'; // introduced JSEncrypt

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  fg: FormGroup;
  constructor(    
    private authService: AuthService,
    private session: SessionService,
    private router: Router,
    // private toast:ToastrService
    ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      login: new FormControl(null, [Validators.required]),
      plainPassword: new FormControl(null, [Validators.required])
    });
  }

  submit() {
    if(this.fg.valid) {
      let user:LoginModel = {login : this.fg.get('login').value, plainPassword : this.fg.get('plainPassword').value};    
      this.authService.login(user).subscribe(data => {    
        if(data)
        {       
          this.session.start(data);
          // this.toast.success("Bienvenue");
          this.router.navigateByUrl('/dashboard')
        }
      //   else
      //     this.toast.error("Echec de connection");
      // }, error => {this.toast.error("Echec de connection");});
    // }
  })}
}
}