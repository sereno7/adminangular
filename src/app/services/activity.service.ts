import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ActivityModel } from '../models/activity'
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  context: BehaviorSubject<ActivityModel[]>
  private url:string = environment.ACTIVIY
  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<ActivityModel[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<ActivityModel[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }
  getAll(){
    return this.http.get<ActivityModel[]>(this.url);
  }

  delete(id:number){
    return this.http.delete(this.url + id);
  }

  add(model:ActivityModel){
    return this.http.post(this.url, model);
  }

  checkActivity(model:ActivityModel){
    let params = new HttpParams();
    if(model) {
      params = params.append("name", model.name)
    }
    return this.http.get<boolean>(this.url + "checkactivity/", {params});
  }

  checkIfCanBeDelete(id:number)
  {
    return this.http.get<boolean>(this.url + "candelete/" + id);
  }

}
