import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginModel} from '../models/login';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import rsa from 'js-crypto-rsa';
// import * as JsEncryptModule from 'jsencrypt'; // introduced JSEncrypt
// import * as forge from 'node-forge'

@Injectable({
  providedIn: 'root'
})
export class AuthService { 
  // rsaKeyValue : any = `-----BEGIN PUBLIC KEY-----
  // MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDbXASdNT6ZwPbDQt+jUa/iu1xJ
  // axAlBM1rqCKjTkA6RX8Sv3DdjXcN2jnAa2MnX3jTZLvUdR43S9sogssl1FIRLkwd
  // J0k9JWs6p2uWX+8tfEPFaz9TRRPVzy7qT86DH2FO1nFjCh5ip2ENuWh/cTvl32fY
  // XRm9/qJNvhwjuPp8ywIDAQAB
  // -----END PUBLIC KEY-----`;
  // rsaJson: any = {"kty":"RSA","e":"AQAB","kid":"333ce413-6f1d-4fed-8985-e71557977677","n":"21wEnTU-mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV_Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll_vLXxDxWs_U0UT1c8u6k_Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs"}
   


  // private publicKey: string;
  url:string = environment.SECURITY;
  constructor(private http: HttpClient) {
    // this.publicKey = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
    // this.encrypt.setPublicKey(this.publicKey);
  }

  login(model: LoginModel): Observable<string> {
    // model.plainPassword = "ikTMnupYD0Vov9+eUD8jQ/lnqaakWq9EVby7hHOtFl3el53QRkQngR929av037ry9D0SMwxMJTwhMultFP3yadmmk9oOYZ37bf9xqhDXJwBpG972HBm4D2tEwFK6i9z8Ddls4gIp/g6M8TLd24ksuarInr8jnVrW2HEtKiJQlSc=";
    // model.plainPassword = btoa(this.getEncrypt(model.plainPassword));
    // model.plainPassword = this.getEncrypt(model.plainPassword);
    // return new Observable(o => {
    //   this.encrypt(model.plainPassword).then(x=>{
    //     model.plainPassword = btoa(new TextDecoder("utf-8").decode(x));
    //     this.http.post<string>(this.url+"login", model).subscribe(data =>{
    //       o.next(data);
    //     });
    //   })
    // });
    
    return this.http.post<string>(this.url+"login", model);
  }

  getPublicKey(){
    this.http.get(this.url+"getpublickey").subscribe(data =>{    
      return data;
    });
  }

  public logOff(id:number){
    return this.http.get<string>(this.url + 'logoff/'+ id ).subscribe();
  }

  // private getEncrypt(pwd:string){
  //   let key = forge.pki.publicKeyFromPem(this.rsaKeyValue);
  //   return key.encrypt(pwd);
  // }

  // createPublicKey(rsaKeyValue:string){
  //   var pem = forge.pki.publicKeyToPem(rsaKeyValue);
  // }
  // encrypt(msg:string){
  //  return rsa.encrypt(
  //     new TextEncoder().encode(msg),
  //     this.rsaJson)
  // }
}


