import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { DayActivitiesModel } from '../models/dayActivities';

@Injectable({
  providedIn: 'root'
})
export class DayActivityService {
  private url:string = environment.DAYACTIVITY
  constructor(private http:HttpClient) { }

  getAll(){
    return this.http.get<DayActivitiesModel[]>(this.url);
  }

  getByIdDay(id:number)
  {
    return this.http.get<DayActivitiesModel[]>(this.url+id);
  }

  add(model:DayActivitiesModel)
  {
    this.http.post(this.url, model).subscribe();
  }

  delete(model:DayActivitiesModel)
  {    
    this.http.post(this.url+"delete/", model).subscribe();
  }
}
