import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DayModel } from '../models/day';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DayService {

  context: BehaviorSubject<DayModel[]>
  private url:string = environment.DAY
  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<DayModel[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<DayModel[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }

  getAll(){
    return this.http.get<DayModel[]>(this.url);
  }
  getById(id:number){
    return this.http.get<DayModel>(this.url + id)
  }

  delete(id:number){
    return this.http.delete(this.url + id);
  }

  update(model:DayModel){
    console.log(model);
    
    return this.http.put<DayModel>(this.url, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  add(model:DayModel){
    console.log(model);
    
    return this.http.post<number>(this.url, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  checkDate(value: string, id: number) {
    console.log(43);
    
    let params = new HttpParams();
    if(value) {
      params = params.append("date", value)
    }
    return this.http.get<boolean>(this.url + 'checkdate/' + id, {params});
  }
}
