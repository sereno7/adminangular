import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { PersonModel} from '../models/person'

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private _user: PersonModel;
  public get user() : PersonModel {
    return this._user;
  }
  constructor(
    // private toast : ToastrService,
    private router : Router) { 
    let token = localStorage.getItem('TOKEN');
    if(token)
    {
      this._user = this.decode(token);   
      // this.toast.warning("Vous êtes connecté sous : "+this.user.name)
      this.router.navigateByUrl('/dashboard')  
    }
  }

  start(token: string) {
    localStorage.setItem('TOKEN', token);
    this._user = this.decode(token);
  }

  stop () {
    // this.toast.info("Vous êtes à présent déconnecté")
    localStorage.clear();
    this._user = null;  
  }

  private decode(token) : PersonModel {
  let decoded = jwt_decode(token);
  return {
    id : decoded["id"],
    name: decoded["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"],
    idTeam: decoded["idTeams"],
    idDay:decoded["idDay"],
    role: decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]
  }
}
}
