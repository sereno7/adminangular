import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TeamModel } from '../models/team'

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private url:string = environment.TEAM
  constructor(private http: HttpClient) { }

  add(team:TeamModel){   
    console.log(team);
    
    return this.http.post(this.url, team);
  }
  delete(id:number){
    return this.http.delete(this.url + id);
  }

  checkTeam(s:string, id:number){
    let params = new HttpParams();
    if(s) {
      params = params.append("name", s+id.toString())
    }
    return this.http.get<boolean>(this.url + "checkteam/", {params});
  }

  // checkTeam(s:string, id:number){
  //   let params = new HttpParams();
  //   if(s) {
  //     params = params.append("name", s)
  //   }
  //   return this.http.get<boolean>(this.url + "checkteam/" + id, {params});
  // }
}
