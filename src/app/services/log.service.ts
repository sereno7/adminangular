import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { LogModel } from '../models/log';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private url:string = environment.LOG;
  constructor(private http:HttpClient) { }

  getAll(){
    return this.http.get<LogModel[]>(this.url);
  }
}
