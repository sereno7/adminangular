import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ActivitiesAverageModel } from '../models/activitiesAverage';
import { ActivitiesMinModel } from '../models/activitiesMin';
import { ActivitiesMaxModel } from '../models/activitiesMax';

@Injectable({
  providedIn: 'root'
})
export class LeaderboardService {
  private url:string = environment.LEADERBOARD;
  constructor(private http:HttpClient) { }

  getAllAverage(){
    return this.http.get<ActivitiesAverageModel[]>(this.url+"getaverage");
  }
  getAllMin(){
    return this.http.get<ActivitiesMinModel[]>(this.url+"getmin");
  }
  getAllMax(){
    return this.http.get<ActivitiesMaxModel[]>(this.url+"getmax");
  }
}
