import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventAddComponent } from './components/event-add/event-add.component';
import { EventListComponent } from './components/event-list/event-list.component';
import { DayResolver} from '../app/resolver/dayResolver';
import { EventUpdateComponent } from './components/event-update/event-update.component';
import { LogComponent } from './components/log/log.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IsAdminGuard } from '../app/guard/is-admin.guard';
import { ActivityComponent } from './components/activity/activity.component';


const routes: Routes = [
  { 
    path: '', 
    component: LoginComponent,
  },
  { 
    path: 'event-list', 
    canActivate: [ IsAdminGuard ],
    component: EventListComponent,
  },
  { 
    path: 'dashboard', 
    canActivate: [ IsAdminGuard ],
    component: DashboardComponent,
  },
  { 
    path: 'event-add',
    canActivate: [ IsAdminGuard ],
    component: EventAddComponent,
  },
  { 
    path: 'log', 
    canActivate: [ IsAdminGuard ],
    component: LogComponent,
  },
  { 
    path: 'activity', 
    canActivate: [ IsAdminGuard ],
    component: ActivityComponent,
  },
  { 
    path: 'event-update/:id', 
    canActivate: [ IsAdminGuard ],
    component: EventUpdateComponent,
    resolve: { resolveDay: DayResolver }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
